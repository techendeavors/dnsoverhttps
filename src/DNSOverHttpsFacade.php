<?php

namespace Techendeavors\DNSOverHttps;

use Illuminate\Support\Facades\Facade;

class DNSOverHttpsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'dnsoverhttps';
    }
}
