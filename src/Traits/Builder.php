<?php

namespace Techendeavors\DNSOverHttps\Traits;

trait Builder
{
    public function geoTargeting(bool $geo)
    {
        $this->geoTargeting = $geo;

        return $this;
    }

    public function padding(bool $enabled, $min = null, $max = null, $chars = null)
    {
        $padding['enabled'] ? $min : $this->padding[0];
        $padding['chars']   ? $min : $this->padding[1];
        $padding['min']     ? $min : $this->padding[2];
        $padding['max']     ? $max : $this->padding[3];

        $this->padding = [
            'enabled'    => $padding['enabled'],
            'characters' => $padding['chars'],
            'minimum'    => $padding['min'],
            'maximum'    => $padding['max'] ];

        return $this;
    }

    public function verifySslCert(bool $verify)
    {
        $this->verifySslCert = $verify;

        return $this;
    }

    public function userAgent(string $agent)
    {
        $this->userAgent = $agent;

        return $this;
    }

    public function dnsServer(string $server)
    {
        $this->dnsServer = $server;

        return $this;
    }

    public function domain(string $domain)
    {
        $this->domains = array_unique(array_merge($this->domains, [$domain]));

        return $this;
    }

    public function domains(array $domainArray)
    {
        $this->domains = array_unique(array_merge($this->domains, $domainArray));

        return $this;
    }

    public function record(string $record)
    {
        array_push($this->records, $record);

        return $this;
    }

    public function records(array $records)
    {
        $this->records = $records;

        return $this;
    }

    public function randomPadding()
    {
        $field = str_shuffle(str_repeat($this->padding['characters'], $this->padding['minimum']));

        return (string) substr($field, 0, rand($this->padding['minimum'], $this->padding['maximum']));
    }

    private function buildResult($collection) {
        $names = array_flatten(array_sort(array_unique(array_pluck($collection, 'name'))));
        $records = array_flatten(array_sort(array_unique(array_pluck($collection, 'type'))));

        $result = [];
        
        foreach ($names as $name) {
            foreach ($records as $record) {
                $records_array[self::idToRr($record)] = $collection->where('name', $name)->where('type', $record)->pluck('data')->toArray();
            }
            $result[] = ['domain' => trim($name, '.'), 'records' => $records_array];
            unset($records_array);
        }

        return $result;
    }
}
