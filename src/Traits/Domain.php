<?php

namespace Techendeavors\DNSOverHttps\Traits;

trait Domain
{
    protected static function decodeIDN($zone)
    {
        return (string) idn_to_ascii($zone, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
    }

    protected static function encodeIDN($zone)
    {
        return (string) idn_to_utf8($zone, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
    }

    protected static function isIDN($zone)
    {
        if (decodeIDN($zone) == $zone) {
            return false;
        }
        
        return true;
    }

    protected static function getTLD($zone)
    {
        return implode(".", array_slice(explode('.', $zone), -1));
    }

    protected static function getDomain($url)
    {
        $results = parse_url($url);
        if (array_key_exists('host', $results)) {
            return $results['host'];
        }

        return $url;
    }

    protected static function getZone($domain)
    {
        return implode(".", array_slice(explode('.', $domain), -2));
    }

    protected static function rrToId($record)
    {
        return array_search(strtoupper($record), self::RECORD_IDS);
    }

    protected static function idToRr($id)
    {
        return self::RECORD_IDS[$id];
    }

    protected static function validRr($record)
    {
        return in_array(strtoupper($record), self::RECORD_IDS);
    }

    protected static function validateId($id)
    {
        return array_key_exists($id, self::RECORD_IDS);
    }
}
