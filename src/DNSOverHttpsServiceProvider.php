<?php

namespace Techendeavors\DNSOverHttps;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class DNSOverHttpsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath($raw = __DIR__.'/../config/dnsoverhttps.php') ?: $raw;

        $this->publishes([$source => config_path('dnsoverhttps.php')], 'config');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $source = realpath($raw = __DIR__.'/../config/dnsoverhttps.php') ?: $raw;

        $this->app->alias('dnsoverhttps', DNSOverHttps::class);

        $this->mergeConfigFrom($source, 'dnsoverhttps');

        $this->app->singleton('dnsoverhttps', function (Container $app) {
            return new DNSOverHttps();
        });        
    }
}
