<?php

namespace Techendeavors\DNSOverHttps;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Support\Collection;
use Techendeavors\DNSOverHttps\Traits\Builder;
use Techendeavors\DNSOverHttps\Traits\Domain;

class DNSOverHttps
{
    use Builder, Domain;

    private const HOSTDEFAULT    = "https://mozilla.cloudflare-dns.com/dns-query";
    private const RECORDSDEFAULT = [
        'A',
        'AAAA',
    ];

    private const GEOTARGETING   = true;
    private const PADDING        = [
        true,
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._',
        10,
        50
    ];

    private const VERIFYSSLCERT  = true;
    private const USERAGENT      = 'Techendeavors-DNSOverHttps-Package';

    private const RECORD_IDS     = [
        1   => 'A',
        2   => 'NS',
        5   => 'CNAME',
        6   => 'SOA',
        12  => 'PTR',
        15  => 'MX',
        16  => 'TXT',
        17  => 'RP',
        24  => 'SIG',
        25  => 'KEY',
        28  => 'AAAA',
        33  => 'SRV',
        35  => 'NAPTR',
        37  => 'CERT',
        39  => 'DNAME',
        43  => 'DS',
        44  => 'SSHFP',
        45  => 'IPSECKEY',
        46  => 'RRSIG',
        47  => 'NSEC',
        48  => 'DNSKEY',
        50  => 'NSEC3',
        51  => 'NSEC3PARAM',
        52  => 'TLSA',
        59  => 'CDS',
        250 => 'TSIG',
        257 => 'CAA',
    ];

    protected $config;
    protected $dnsServer;
    protected $domains;
    protected $geoTargeting;
    protected $host;
    protected $padding;
    protected $records;
    protected $requests;
    protected $responses;
    protected $result;
    protected $userAgent;
    protected $verifySslCert;

    /**
     * Classes which have a constructor method call this method on each newly-created object, 
     * so it is suitable for any initialization that the object may need before it is used.
     */
    public function __construct()
    {
        $this->domains               = [];
        $this->config                = config('dnsoverhttps');
        $this->geoTargeting          = $this->config['geoTargeting']                            ?: self::GEOTARGETING;
        $this->host                  = $this->config['hosts'][$this->config['hostDefault']]     ?: self::HOSTDEFAULT;
        $this->padding               = [ 'enabled'    => $this->config['padding']['enabled']    ?: self::PADDING[0],
                                         'characters' => $this->config['padding']['characters'] ?: self::PADDING[1],
                                         'minimum'    => $this->config['padding']['minimum']    ?: self::PADDING[2],
                                         'maximum'    => $this->config['padding']['maximum']    ?: self::PADDING[3]
                                       ];
        $this->records               = $this->config['recordsDefault']                          ?: self::RECORDSDEFAULT;
        $this->userAgent             = $this->config['userAgent']                               ?: self::USERAGENT;
        $this->verifySslCert         = $this->config['verifySslCert']                           ?: self::VERIFYSSLCERT;
        $this->requests              = [];
        $this->responses             = [];
        $this->result                = [];
    }

    /**
     * The __invoke() method is called when a script tries to call an object as a function.
     * https://secure.php.net/manual/en/language.oop5.magic.php#object.invoke
     */
    public function __invoke($domains)
    {
        var_dump($domains); // example
    }

    /**
     * The __toString() method allows a class to decide how it will react when it is treated like a string. For 
     * example, what echo $obj; will print. This method must return a string, as otherwise a fatal E_RECOVERABLE_ERROR 
     * level error is emitted.
     * https://secure.php.net/manual/en/language.oop5.magic.php#object.tostring
     */
    public function __toString()
    {
        return (string) implode(",", $this->domains);
    }

    public function generateQuery(string $domain, string $record)
    {
        $query['name'] = $domain;
        $query['type'] = strtoupper($record);
        if ($this->geoTargeting == false) {
            $query['edns_client_subnet'] = '0.0.0.0/0';
        }
        if ($this->padding['enabled'] == true) {
            $query['random_padding'] = static::randomPadding();
        }
        $fullquery = $this->host."?".http_build_query($query);

        return (string) $fullquery;
    }

    public function check()
    {
        $clientConfig = [
            'base_uri'         => $this->host,
            'allow_redirects'  => $this->config['hostFollowRedirects'] ?: true,
            'connect_timeout'  => $this->config['hostConnectTimeout'] ?: 10,
            'force_ip_resolve' => 'v4',
            'headers'          => [
                'accept'     => 'application/dns-json',
                'User-Agent' => $this->userAgent,
            ],
            'timeout'          => $this->config['hostReplyTimeout'] ?: 10,
            'verify'           => $this->verifySslCert,
        ];

        $client = new Client($clientConfig);
        
        reset($this->domains);
        reset($this->records);

        $promises = [];

        foreach ($this->domains as $domain) {
            foreach ($this->records as $record) {
                $query = self::generateQuery($domain, $record);
                $this->requests[] = $query;
                $promises[] = $client->requestAsync('GET', $query);
            }
        }

        Promise\all($promises)->then(function (array $responses) {
            foreach ($responses as $response) {
                $this->responses[] = json_decode($response->getBody(), true);
            }
        })->wait();
        
        $combined = collect(array_collapse(array_pluck($this->responses, 'Answer')))
            ->sortBy('type')
            ->sortBy('name')
            ->unique(function ($item) { 
                return $item['name'].$item['type'].$item['data']; 
            });

        return self::buildResult($combined);
    }

    /**
     * This method is called by var_dump() when dumping an object to get the properties that should be shown. 
     * If the method isn't defined on an object, then all public, protected and private properties will be shown.
     */
    public function __debugInfo()
    {
        $debugInfo = [
            'dnsServer'     => $this->dnsServer,
            'domains'       => $this->domains,
            'geoTargeting'  => $this->geoTargeting,
            'host'          => $this->host,
            'padding'       => $this->padding,
            'records'       => $this->records,
            'requests'      => $this->requests,
            'responses'     => $this->responses,
            'result'        => $this->result,
            'userAgent'     => $this->userAgent,
            'verifySslCert' => $this->verifySslCert,
        ];

        if ($this->config['debug']) {
            return $debugInfo;
        }
        
        return null;
    }

    /**
     * __call() is triggered when invoking inaccessible methods in an object context.
     * $obj = new MethodTest;
     * $obj->runTest('in object context');
     *
     * Returns: Calling object method 'runTest' in object context
     */
    public function __call($name, $arguments)
    {
        // Note: value of $name is case sensitive.
        echo "Calling object method '$name' "
             . implode(', ', $arguments). "\n";
    }

    /**
     * __call() is triggered when invoking inaccessible methods in a static context.
     * MethodTest::runTest('in static context');
     *
     * Returns: Calling static method 'runTest' in static context
     */
    public static function __callStatic($name, $arguments)
    {
        // Note: value of $name is case sensitive.
        echo "Calling static method '$name' "
             . implode(', ', $arguments). "\n";
    }
}
