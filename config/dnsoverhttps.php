<?php

return [

/**
 * DNSOverHttps Settings
 */

    // Array of DNS-Over-HTTPS APIs available
    'hosts' => [
        'cloudflare' => 'https://cloudflare-dns.com/dns-query',
        'mozilla'    => 'https://mozilla.cloudflare-dns.com/dns-query',
        'google'     => 'https://dns.google.com/resolve'
    ],

    // Which DNS-Over-HTTPS API to use for queries
    'hostDefault'    => 'mozilla',

    // If the DNS-Over-HTTPS APi doesn't reply quickly enough, should we try the query again with another host?
    'hostFallback'        => true,
    'hostConnectTimeout'  => 10,
    'hostReplyTimeout'    => 10,
    'hostFollowRedirects' => true,

    // Resource Records available through the API
    'recordsAvailable'    => [
        'a',
        'aaaa',
        'cname',
        'dnskey',
        'ds',
        'mx',
        'ns',
        'ptr',
        'soa',
        'srv',
        'txt'
    ],

    // If a record type isn't specified, what resource records should we query?
    'recordsDefault' => [
        'a',
        'aaaa'
    ],

    // DNS servers sometimes give different results depending on your IP address and apparent geographic location
    'geoTargeting' => true,

    // We can append random data to your request to make it difficult for network snoopers to try to guess what
    // domain you are querying simply because of the request size.
    'padding' => [
        'enabled'    => true,
        'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._',
        'minimum'    => 10,
        'maximum'    => 50
    ],

    // Verify host SSL certificate
    'verifySslCert' => true,

    // User agent to report to the host API
    'userAgent' => 'Techendeavors-DNSOverHttps-Package',

    // If Debugging is enabled, information about the object is returned in a Tinker session while you build it.
    'debug' => true,
];
