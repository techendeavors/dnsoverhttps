# DNSOverHttps

## DNSOverHttps allows you to query DNS records over an HTTPS connection rather than interacting with your local DNS server

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/dnsoverhttps.svg?style=flat-square)](https://packagist.org/packages/techendeavors/dnsoverhttps)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/dnsoverhttps.svg?style=flat-square)](https://packagist.org/packages/techendeavors/dnsoverhttps)

DNSOverHttps allows you to query DNS records over an HTTPS connection rather than interacting with your local DNS server. Supports the public Mozilla, Cloudflare, and Google API servers.

The DnsOverHttps servers only support requesting a single record type at a time, but we handle that for you. Just pass an array of record types that you want and the package will query them all and merge them back together into one single object. 

While the DNS Over Https servers do a lot of the work, we've also bundled a few extra client-side utilities that might be helpful. 

### To Do

- [ ] Build 'techendeavors/tldverify` package and integrate it
- [ ] Allow overriding the DNS Over Https host in the builder
- [ ] Make returned data a little easier to work with. 
- [X] Set Guzzle to query in parallel use promises when more than one RR is requested rather than making the request in a serial fashion
- [X] Combine all guzzle promise responses into one single object
- [ ] Allow for distributing requests across multiple hosts rather than just using the primary host
- [X] Allow for package configuration to override constants.
- [ ] Build some validators (DnsHasA, DnsHasAAAA, DnsHasMX, DnsHasSoa, DnsHasNs, etc)
- [ ] Write Tests
- [ ] If Guzzle is not installed, fall back to Curl. Change guzzle requirement to a suggested package. 

### Installation

You can install the package via composer:

```bash
composer require techendeavors/dnsoverhttps
```

### Usage

The most basic. `A` and `AAAA` records are assumed unless overridden.
```
$domain = new DNSOverHttps;
$domain->domain('example.com');
$domain->check();
=> [
     [
       "domain" => "example.com",
       "records" => [
         "A" => [
           "93.184.216.34",
         ],
         "AAAA" => [
           "2606:2800:220:1:248:1893:25c8:1946",
         ],
       ],
     ],
   ]
```

A bit more advanced. 
```
$domain = new DNSOverHttps();
$domain->domains(['example.com', 'techendeavors.com', 'packagist.org']);
$domain->records(['A', 'AAAA', 'MX', 'CNAME', 'TXT']);
$domain->check();
=> [
     [
       "domain" => "example.com",
       "records" => [
         "A" => [
           "93.184.216.34",
         ],
         "MX" => [],
         "TXT" => [
           ""v=spf1 -all"",
           ""$Id: example.com 4415 2015-08-24 20:12:23Z davids $"",
         ],
         "AAAA" => [
           "2606:2800:220:1:248:1893:25c8:1946",
         ],
       ],
     ],
     [
       "domain" => "packagist.org",
       "records" => [
         "A" => [
           "142.44.164.255",
         ],
         "MX" => [
           "10 aspmx2.googlemail.com.",
           "5 alt2.aspmx.l.google.com.",
           "5 alt1.aspmx.l.google.com.",
           "10 aspmx3.googlemail.com.",
           "10 aspmx4.googlemail.com.",
           "10 aspmx5.googlemail.com.",
           "1 aspmx.l.google.com.",
         ],
         "TXT" => [
           ""v=spf1 a mx mx:ns373883.ip-5-196-92.eu include:_spf.google.com include:amazonses.com ~all"",
           ""google-site-verification=u1ogg4eabdx_nFHnTHC0s4KfKUkSzaME6FIbDj9NU1Y"",
         ],
         "AAAA" => [
           "2607:5300:201:2100::7:2274",
         ],
       ],
     ],
     [
       "domain" => "techendeavors.com",
       "records" => [
         "A" => [
           "104.28.3.33",
           "104.28.2.33",
         ],
         "MX" => [
           "20 eagle-relay.mxlogin.com.",
           "10 eagle.mxlogin.com.",
         ],
         "TXT" => [
           ""keybase-site-verification=qUHhd_PYnbKj0SQIzOZuv-LIhPW8wLoCgMZ3Z6nf8A0"",
           ""keybase-site-verification=5sU4NgQQ1slHOkwKahEVR19_TFm6XQULunmqbtTIXgc"",
           ""v=spf1 include:mxlogin.com -all"",
         ],
         "AAAA" => [
           "2400:cb00:2048:1::681c:221",
           "2400:cb00:2048:1::681c:321",
         ],
       ],
     ],
   ]
```


### Testing
Not yet implemented

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/dnsoverhttps)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)

### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
